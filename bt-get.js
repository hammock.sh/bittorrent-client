const fs = require('fs')
const dgram = require('dgram')
// Buffer is available globally by default, so we don't need this line
// const Buffer = require('buffer').Buffer

function main() {
  let torrent_file_path = process.argv[2]
  if (!torrent_file_path) {
    console.error('Usage: bt-get <torrent-file>')
    process.exit(1)
  }
  if (!fs.existsSync(torrent_file_path)) {
    console.error(torrent_file_path+' does not exist')
    process.exit(1)
  }
  let bencoded_torrent_data = fs.readFileSync(torrent_file_path)
  let torrent_data = decode_torrent(bencoded_torrent_data)
  console.log('The torrent you are trying to download:')
  console.log(torrent_data)
  connect(process.env.TRACKER || 'tracker.example.com', process.env.TRACKER_PORT || 6969)
}

function decode_torrent(data_string) {
  let torrent = parse_dictionary(data_string).value
  torrent.announce = torrent.announce.toString()
  torrent['announce-list'] = torrent['announce-list'].map(buffer => {
    return buffer.toString()
  })
  torrent['created by'] = torrent['announce-list'].toString()
  torrent['url-list'] = torrent['announce-list'].map(buffer => {
    return buffer.toString()
  })
  torrent.comment = torrent.comment.toString()
  torrent.encoding = torrent.encoding.toString()
  torrent.info.name = torrent.info.name.toString()
  torrent.info.files = torrent.info.files.map(file => {
    file.path = file.path.toString()
    return file
  })
  return torrent
}

function parse_unknown(data) {
  let first_char = char_at(data, 0)
  if (first_char === 'd') {
    return parse_dictionary(data)
  }
  if (first_char === 'l') {
    return parse_list(data)
  }
  if (first_char === 'i') {
    return parse_number(data)
  }
  if (is_byte_string(data)) {
    return parse_byte_string(data)
  }
  throw new Error('Encountered unknown token: '+data.slice(0, 30).toString())
}

let digits = '0123456789'
function is_byte_string(data) {
  for (let i = 0; i < data.length; i++) {
    let char = char_at(data, i)
    // ':' cannot be the first character in data
    // If this ever becomes true, then we know that every character
    // that we found up to this point was a number.
    // If we encountered a non-number, the function would have returned
    if (i > 0 && char === ':') {
      return true
    }
    if (!digits.includes(char)) {
      return false
    }
  }
  throw new Error('Found only digits: '+data.slice(0, 50))
}

function char_at(buffer, index) {
  return buffer.toString('utf8', index, index+1)
}

function parse_byte_string(data) {
  let number_end = data.indexOf(':')
  let string_start = number_end + 1
  let string_length = parseInt(data.slice(0, number_end))
  let value = data.slice(string_start, string_start + string_length)
  let length = string_start + string_length
  return {value: value, length: length}
}

function parse_number(data) {
  let start = 1
  let end = data.indexOf('e')
  if (end === -1) {
    throw new Error('Number did not end correctly: '+data.slice(0, 30))
  }
  let length = end + 1
  let value = parseInt(data.slice(start, end))
  return {value: value, length: length}
}

function parse_list(data) {
  let length = 2 // list is wrapped in `l/e`, so the length is at least 2
  let list_value = []
  data = data.slice(1) // discard the first 'l'
  while (char_at(data, 0) !== 'e') {
    if (!data.length) {
      throw new Error('Never encountered the end of the list')
    }
    let next_value = parse_unknown(data)
    list_value.push(next_value.value)
    length += next_value.length
    data = data.slice(next_value.length)
  }
  return {value: list_value, length: length}
}

function parse_dictionary(data) {
  let length = 2 // list is wrapped in `d/e`, so the length is at least 2
  let dict_value = {}
  data = data.slice(1) // discard the first 'd'
  let key
  while (char_at(data, 0) !== 'e') {
    if (!data.length) {
      throw new Error('Never encountered the end of the dictionary')
    }
    let next_value = parse_unknown(data)
    length += next_value.length
    data = data.slice(next_value.length)
    if (key) {
      dict_value[key] = next_value.value
      key = null
    } else {
      key = next_value.value
    }
  }
  return {value: dict_value, length: length}
}

function int_buffer(int) {
  let b = Buffer.alloc(4)
  b[0] = int >> 24
  b[1] = (int >> 16) & 0xff
  b[2] = (int >> 8) & 0xff
  b[3] = int & 0xff
  return b
}

function connect(tracker_hostname, tracker_port) {
  let transaction_id = Math.round(Math.random()*0xffffffff)
  let connect_message = Buffer.concat([
    int_buffer(0x417), // first half of protocol id
    int_buffer(0x27101980), // second half of protocol id
    int_buffer(0), // action
    int_buffer(transaction_id),
  ])
  let socket = dgram.createSocket('udp4')
  socket.on('message', function(message) {
    console.log('Incoming: '+message)
  })
  function on_send(err) {
    if (err) {
      console.error('Message failed to send: '+err)
    } else {
      console.log('Started: '+transaction_id.toString(16))
    }
  }
  socket.send(connect_message, tracker_port, tracker_hostname, on_send)
}

main()
