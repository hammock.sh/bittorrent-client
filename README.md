# Let's build a bittorrent client

The code here is meant to be a working reference for the code in the
[hammock.sh BitTorrent client series](https://hammock.sh/tag/bittorrent-client/)
